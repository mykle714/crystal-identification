import cv2 as cv #OpenCV
import numpy as np

def imgShow(img, i=1, last=True):
	cv.imshow("image"+str(i),img)
	if(last):
		cv.waitKey(0)
		cv.destroyAllWindows()

#Looks for the background by registering any line of pixels with a small variance as just noise from the background.
#modify the resoution factor l and the threshold standard deviation, t depending on the image quality.
def background(img, l=5, t=2):
	ret = np.zeros([len(img)-l, len(img[0])-l])
	c = 0
	line = [0]*l

	for i in range(len(ret)):
		flag = False
		for j in range(len(ret[0])):
			line[c] = img[i][j]
			if(c == l-1):
				flag = True
			c = (c + 1)%l
			if(flag):
				std = np.std(line)
				if(std < t):
					ret[i][j] = 255

	c = 0
	line = [0]*l
	for i in range(len(ret[0])):
		flag = False
		for j in range(len(ret)):
			line[c] = img[j][i]
			if(c == l-1):
				flag = True
			c = (c + 1)%l
			if(flag):
				std = np.std(line)
				if(std < t):
					ret[j][i] = 255

	return ret

#Looks for edges by comparing pixels with adjacent pixels. If the difference is large, then register as an edge
#not used, didn't work as well as finding the background
def edges(img, p=2.4):
	ret = np.empty([len(img)-2, len(img[0])-2])

	for i in range(1,len(ret)):
		for j in range(1,len(ret[0])):
			for k in range(1,3):
				#measures error
				right = abs(img[i][j+k] - img[i][j]) / max(img[i][j+k],img[i][j])
				down  = abs(img[i+k][j] - img[i][j]) / max(img[i+k][j],img[i][j])
				left  = abs(img[i][j-k] - img[i][j]) / max(img[i][j-k],img[i][j])
				up    = abs(img[i-k][j] - img[i][j]) / max(img[i-k][j],img[i][j])

				#array values all less than 1 (indicating % error)
				if(right > p or down > p or left > p or up > p):
					ret[i][j] = 0
					break
				else:
					ret[i][j] = 255
	return ret

#finds consecutive pixels
def flood(img, ret, i, j, counter, coords):
	ret[i][j] = counter
	if not counter in coords:
		coords[counter] = []
	
	coords[counter].append((i,j))

	next = []
	for x in [(0,1),(1,0),(0,-1),(-1,0)]:
		n, m = x
		if i+n >= 0 and j+m >= 0 and i+n < len(img) and j+m < len(img[0]) and img[i+n][j+m] == 0 and ret[i+n][j+m] == 0:
			next.append((i+n,j+m))

	for x in next:
		flood(img,ret,x[0],x[1],counter,coords)

#locates the coordinates pixels composing each crystal
#modify the minimal allowed crystal size in pixels (Area)
def locate(img, minSize=100):
	ret = np.zeros([len(img), len(img[0])])
	c = 1
	coords = {}

	for i in range(len(img)):
		for j in range(len(img[0])):
			if img[i][j] == 0 and ret[i][j] == 0:
				flood(img,ret,i,j,c,coords)
				if(len(coords[c]) < minSize):
					coords.pop(c, None)
				else:
					c += 1

	#more expensive method O(i*n)
	locations = {}
	for i in coords:
		sumi = 0
		sumj = 0
		for n,m in coords[i]:
			sumi += n
			sumj += m
		locations[i] = (int(sumi/len(coords[i])),int(sumj/len(coords[i])))

	# cheaper method by just finding the midpoint between mins and maxes O(i)
	# locations = {}
	# for i in coords:
	# 	n = sorted(coords[i], key=lambda x: x[0])
	# 	maxi = n[-1][0]
	# 	mini = n[0][0]
		
	# 	m = sorted(coords[i], key=lambda x: x[1])
	# 	maxj = m[-1][1]
	# 	minj = m[0][1]

	# 	midi = (maxi + mini)/2
	# 	midj = (maxj + minj)/2
	# 	locations[i] = (midi,midj)

	return locations

#plots the red location crosshairs
def plotCoords(ret, coords):
	for x in coords:
		i,j = coords[x]
		i = int(i)
		j = int(j)
		ret[i][j] = (255,0,0)
		for y in range(3):
			if i+y < len(ret): ret[i+y][j] = [0,0,255]
			if j+y < len(ret[0]): ret[i][j+y] = [0,0,255]
			if i-y >= 0: ret[i-y][j] = [0,0,255]
			if j-y >= 0: ret[i][j-y] = [0,0,255]

	return ret

img_name = "test1"
path = "images/"+img_name+".jpg"
orig = cv.imread(path)
img =  cv.cvtColor(orig, cv.COLOR_BGR2GRAY)
bac = background(img)
cv.imwrite( "images/"+img_name+"-1.jpg", bac)

coords = locate(bac)
plot = plotCoords(orig,coords)
cv.imwrite( "images/"+img_name+"-2.jpg", plot)

imgShow(img,1,False)
imgShow(bac,2,False)
imgShow(plot,3)